<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompanyController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('companies', [CompanyController::class, 'index'])->name('companies');
    Route::get('companies/create', [CompanyController::class, 'form'])->name('company.form');
    Route::get('companies/edit/{id}', [CompanyController::class, 'formEdit'])->name('company.form.edit');
    Route::get('companies/{id}', [CompanyController::class, 'show'])->name('company.show');
    Route::post('save_company', [CompanyController::class, 'store'])->name('company.save');
    Route::put('update_company', [CompanyController::class, 'edit'])->name('company.edit');
    Route::delete('delete_company', [CompanyController::class, 'destroy'])->name('company.destroy');
});

require __DIR__.'/auth.php';
