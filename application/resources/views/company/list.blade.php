<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Garages') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <a href="{{ route('company.form') }}" class="p-5 bg-success" >CREER UN GARAGE</a>
                </div>
            </div>
        </div>
        
    </div>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <table class="min-w-full divide-y divide-gray-200">
                <thead class="bg-gray-50">
                    <tr>
                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">#</th>
                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Name</th>
                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Social reason</th>
                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">address</th>
                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Phone 1</th>
                        {{-- <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Phone 2</th> --}}
                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Email</th>
                        {{-- <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">RCA</th> --}}
                        {{-- <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Trade register</th> --}}
                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Edit</th>
                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Delete</th>
                    </tr>
                </thead>
                <tbody class="bg-white divide-y divide-gray-200">
                    @php $i = 0; @endphp
                    @forelse($companies as $company)
                        @php $i++; @endphp
                        <tr>
                            <td>
                                <div class="ml-4">
                                    <div class="text-sm font-medium text-gray-900">{{ $i }}</div>
                                </div>
                            </td>
                            <td>
                                <div class="ml-4">
                                    <div class="text-sm font-medium text-gray-900">{{ $company->name }}</div>
                                </div>
                            </td>
                            <td>
                                <div class="ml-4">
                                    <div class="text-sm font-medium text-gray-900">{{ $company->social_reason }}</div>
                                </div>
                            </td>
                            <td>
                                <div class="ml-4">
                                    <div class="text-sm font-medium text-gray-900">{{ $company->address }}</div>
                                </div>
                            </td>
                            <td>
                                <div class="ml-4">
                                    <div class="text-sm font-medium text-gray-900">{{ $company->phone }}</div>
                                </div>
                            </td>
                            {{-- <td>
                                <div class="ml-4">
                                    <div class="text-sm font-medium text-gray-900">{{ $company->phone2 }}</div>
                                </div>
                            </td> --}}
                            <td>
                                <div class="ml-4">
                                    <div class="text-sm font-medium text-gray-900">{{ $company->email  }}</div>
                                </div>
                            </td>
                            {{-- <td>
                                <div class="ml-4">
                                    <div class="text-sm font-medium text-gray-900">{{ $company->rca }}</div>
                                </div>
                            </td> --}}
                            {{-- <td>
                                <div class="ml-4">
                                    <div class="text-sm font-medium text-gray-900">{{ $company->trade_register }}</div>
                                </div>
                            </td> --}}
                            <td>
                                <div class="ml-4">
                                    <div class="text-sm font-medium text-gray-900"><a href="{{ route('company.form.edit', ['id' => $company->id]) }}" class="p-5 text-info" > <span>Edit</span> </a></div>
                                </div>
                            </td>
                            <td>
                                <div class="ml-4">
                                    <div class="text-sm font-medium text-gray-900"><a href="{{ route('company.destroy', ['id' => $company->id]) }}" class="p-5 text-info" > <span>Delete</span> </a></div>
                                </div>
                            </td>
                        </tr>
                    @empty
                        
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</x-app-layout>