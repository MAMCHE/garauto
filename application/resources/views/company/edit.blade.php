<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="PUT" action="{{ route('company.edit') }}">
            @csrf
            <!-- Name -->
            <div>
                <x-label for="name" :value="__('Name')" />
                <x-input id="name" class="block mt-1 w-full" type="text" name="name" value="{{ $company->name }}" required autofocus />
            </div>

            <!-- social reason -->
            <div>
                <x-label for="social_reason" :value="__('Social reason')" />
                <x-input id="social_reason" class="block mt-1 w-full" type="text" name="social_reason" value="{{ $company->social_reason }}" required  />
            </div>

            <!-- address -->
            <div>
                <x-label for="address" :value="__('Address')" />
                <x-input id="address" class="block mt-1 w-full" type="text" name="address" value="{{ $company->address }}" required  />
            </div>

            <!-- phone -->
            <div>
                <x-label for="phone" :value="__('Phone 1')" />
                <x-input id="phone" class="block mt-1 w-full" type="text" name="phone" value="{{ $company->phone }}" required  />
            </div>

            <!-- phone2 -->
            <div>
                <x-label for="phone2" :value="__('Phone 2')" />
                <x-input id="phone2" class="block mt-1 w-full" type="text" name="phone2" value="{{ $company->phone2 }}" />
            </div>

            <!-- Email Address -->
            <div class="mt-4">
                <x-label for="email" :value="__('Email')" />
                <x-input id="email" class="block mt-1 w-full" type="email" name="email"  value="{{ $company->email }}" required />
            </div>

            <!-- rca -->
            <div>
                <x-label for="rca" :value="__('RCA')" />
                <x-input id="rca" class="block mt-1 w-full" type="text" name="rca" value="{{ $company->rca }}" />
            </div>

            <!-- trade_register -->
            <div>
                <x-label for="trade_register" :value="__('Trade register')" />
                <x-input id="trade_register" class="block mt-1 w-full" type="text" name="trade_register" value="{{ $company->trade_register }}" />
            </div>

            <input type="hidden" name="id" value="{{ $company->id }}">

            <div class="flex items-center justify-end mt-4">
                <x-button class="ml-4">
                    {{ __('Modifier ce garage') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>
