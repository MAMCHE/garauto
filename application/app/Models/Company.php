<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

     /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'matricule',
        'name',
        'social_reason',
        'address',
        'phone',
        'phone2',
        'email',
        'rca',
        'trade_register',
        'logo',
        'is_active',
    ];
}
