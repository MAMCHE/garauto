<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\CompanyService;
use App\Entities\CompanyEntity;

class CompanyController extends Controller
{
    private CompanyService $companyService;

    public function __construct(CompanyService $companyService) {
        $this->companyService = $companyService;
    }

    public function index(){
        $companies = $this->companyService->getCompanies();
        return view("company.list", ["companies" => $companies]);
    }

    public function form(){
        return view("company.form");
    }

    public function formEdit(int $id){
        $company = $this->companyService->getCompanyById($id);
        return view("company.edit", ["company" => $company]);
    }

    public function show(int $id){
        $company = $this->companyService->getCompanyById($id);
        return view("company.show", ["company" => $company]);
    }

    //
    public function store(Request $request){
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
        ]);

        $companyData = [
            "name" => $request->name,
            "social_reason" => $request->social_reason,
            "address" => $request->address,
            "phone" => $request->phone,
            "phone2" => $request->phone2,
            "email" => $request->email,
            "rca" => $request->rca,
            "trade_register" => $request->trade_register,
        ];

        $company = new CompanyEntity($companyData);

        $this->companyService->createCompany($company);
        return redirect()->route('companies');
    }

    public function edit(Request $request){
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
        ]);

        $companyData = [
            "id" => $request->id,
            "name" => $request->name,
            "social_reason" => $request->social_reason,
            "address" => $request->address,
            "phone" => $request->phone,
            "phone2" => $request->phone2,
            "email" => $request->email,
            "rca" => $request->rca,
            "trade_register" => $request->trade_register,
        ];

        $company = new CompanyEntity($companyData);

        $this->companyService->updateCompany($company);
        return redirect()->route('companies');
    }

    public function destroy(int $id){
        $company = $this->companyService->deleteCompany($id);
        return view("company.show", ["company" => $company]);
    }
}
