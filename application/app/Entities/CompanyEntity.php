<?php

    namespace App\Entities;

    class CompanyEntity {

        private int $id;
        private string $matricule;
        private string $name;
        private string $social_reason;
        private string $address;
        private string $phone;
        private string $phone2;
        private string $email;
        private string $rca;
        private string $trade_register;
        private string $logo;
        private string $is_active;

        public function hydrate(array $data){
			foreach($data as $key => $value){
				$method = 'set'.ucfirst($key);
				if(method_exists($this, $method)){
					$this->$method($value);
				}
			}
		}

        // constructor

        public function __construct(array $data){
			$this->hydrate($data);
		}

        // getters

        public function getId(): int {
            return $this->id;
        }

        public function getMatricule(): string{
            return $this->matricule;
        }

        public function getName(): string{
            return $this->name;
        }

        public function getSocial_reason(): string{
            return $this->social_reason;
        }

        public function getAddress(): string{
            return $this->address;
        }

        public function getPhone(): string{
            return $this->phone;
        }

        public function getPhone2(): string{
            return $this->phone2;
        }

        public function getEmail(): string{
            return $this->email;
        }

        public function getRca(): string{
            return $this->rca;
        }

        public function getTrade_register(): string{
            return $this->trade_register;
        }

        public function getLogo(): string{
            return $this->logo;
        }

        public function getIs_active(): string{
            return $this->is_active;
        }

        // setters
    
        public function setId(int $id): void {
            $this->id = (int) $id;
        }

        public function setMatricule(string $matricule): void {
            $this->matricule = $matricule;
        }

        public function setName(string $name): void {
            $this->name = (string) $name;
        }

        public function setSocial_reason(string $social_reason): void {
            $this->social_reason = (string) $social_reason;
        }

        public function setAddress(string $address): void {
            $this->address = (string) $address;
        }

        public function setPhone(string $phone): void {
            $this->phone = (string) $phone;
        }

        public function setPhone2(string $phone): void {
            $this->phone2 = (string) $phone;
        }

        public function setEmail(string $email): void {
            $this->email = (string) $email;
        }

        public function setRca(string $rca): void {
            $this->rca = (string) $rca;
        }

        public function setTrade_register(string $trade_register): void {
            $this->trade_register = (string) $trade_register;
        }

        public function setLogo(string $logo): void {
            $this->logo = (string) $logo;
        }

        public function setIs_active(bool $is_active): void {
            $this->is_active = (string) $is_active;
        }
    }

