<?php

    namespace App\Services;

    use App\Models\Company;
    use App\Entities\CompanyEntity;
    use App\Dao\CompanyDao;

    class CompanyService {

        private CompanyDao $companyDao;

        public function __construct(CompanyDao $companyDao) {
            $this->companyDao = $companyDao;
        }

        /**
         * Service to generate a company's matricule
         * @var string $name
         * @return string $result
         */
        public function generateMatricule(string $name): string{
            $randon_number = rand(111111, 999999);
            return strtoupper(substr($name, 0, 2)).$randon_number;
        }

        /**
         * Service to create a company
         * @var CompanyEntity $company
         * @return Company $result
         */
        public function createCompany(CompanyEntity $company): Company{
            // Generate matricule
            $company->setMatricule($this->generateMatricule($company->getName()));
            return $this->companyDao->createCompany($company);
        }

        /**
         * Service to get companies
         * @return Company[] $result
         */
        public function getCompanies(){
            return $this->companyDao->getCompanies();
        }

        /**
         * Service to get a company by id
         * @var int $id
         * @return Company $result
         */
        public function getCompanyById(int $id){
            return $this->companyDao->getCompanyById($id);
        }

        /**
         * Service to update a company
         * @var CompanyEntity $company
         * @return bool $result
         */
        public function updateCompany(CompanyEntity $companyEntity){
           return $this->companyDao->updateCompany($companyEntity);
        }

        /**
         * Service to delete a company
         * @var int $id
         * @return bool $result
         */
        public function deleteCompany(int $id){
            return $this->companyDao->deleteCompany($id);
        }
    }