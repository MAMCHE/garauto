<?php

    namespace App\Dao;

    use App\Entities\CompanyEntity;
    use App\Models\Company;

    class CompanyDao {

        /**
         * @var Company
         */
        private Company $company;

        public function __construct(Company $company) {
            $this->company = $company;
        }

        /**
         * Dao to create a company
         * @var CompanyEntity $companyEntity
         * @return Company $result
         */
        public function createCompany(CompanyEntity $companyEntity): Company {
            return $this->company::create([
                'matricule' => $companyEntity->getMatricule(),
                'name' => $companyEntity->getName(),
                'social_reason' => $companyEntity->getSocial_reason(),
                'address' => $companyEntity->getAddress(),
                'phone' => $companyEntity->getPhone(),
                'phone2' => $companyEntity->getPhone2(),
                'email' => $companyEntity->getEmail(),
                'rca' => $companyEntity->getRca(),
                'trade_register' => $companyEntity->getTrade_register(),
            ]);
        }

        /**
         * Dao to update a company
         * @var CompanyEntity $companyEntity
         * @return bool $result
         */
        public function updateCompany(CompanyEntity $companyEntity): bool {
            $company = $this->getCompanyById($companyEntity->getId());

            if($company){
                return $company->update([
                    'matricule' => $companyEntity->getMatricule(),
                    'name' => $companyEntity->getName(),
                    'social_reason' => $companyEntity->getSocial_reason(),
                    'address' => $companyEntity->getAddress(),
                    'phone' => $companyEntity->getPhone(),
                    'phone2' => $companyEntity->getPhone2(),
                    'email' => $companyEntity->getEmail(),
                    'rca' => $companyEntity->getRca(),
                    'trade_register' => $companyEntity->getTrade_register(),
                ]);
            }
            return null;
        }

        /**
         * Dao to get a company by id
         * @var int $id
         * @return Company $result
         */
        public function getCompanyById(int $id): Company {
            return $this->company::findOrFail($id);
        }

        /**
         * Dao to delete a company
         * @var int $id
         * @return bool $result
         */
        public function deleteCompany(int $id): bool {
            $company = $this->getCompanyById($id);

            if($company){
                return $company->delete();
            }
            return null;
        }

        /**
         * Dao to get a collection of companies
         * @return Company[] $result
         */
        public function getCompanies(){
            return $this->company::where(['is_active' => true])->get();
        }
    }